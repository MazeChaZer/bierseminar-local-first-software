.PHONY: paper
paper: graphics/cloud-collaboration-architecture.pdf
paper: graphics/future.pdf
paper: graphics/local-first-collaboration-architecture.pdf
	latexmk

.PHONY: clean
clean:
	rm graphics/*.pdf
	latexmk -C

graphics/cloud-collaboration-architecture.pdf: graphics/cloud-collaboration-architecture.drawio
	drawio --export --output $@ $<

graphics/future.pdf: graphics/future.drawio
	drawio --export --output $@ $<

graphics/local-first-collaboration-architecture.pdf: graphics/local-first-collaboration-architecture.drawio
	drawio --export --output $@ $<
