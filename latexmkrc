$latex = 'latex  %O  --shell-escape %S';
$pdflatex = 'pdflatex  %O  --shell-escape %S';
$pdf_mode = 1;
$out_dir = 'out';
@default_files = ('main.tex');
