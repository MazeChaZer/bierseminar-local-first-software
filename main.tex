\documentclass[11pt,xcolor=dvipsnames,aspectratio=169]{beamer}
% [aspectratio=169]
\include{preambel}

\usepackage{biblatex}
\addbibresource{literature.bib}
\usepackage{svg}

\title{Local-First Software}
\subtitle{You Own Your Data, in Spite of the Cloud}
\author{Jonas Schürmann}
\date{2021-12-03}
\institute[TU Dortmund University]{Chair for Programming Systems,\\ TU Dortmund University}
\titlegraphic{\hfill\includegraphics[height=8mm]{graphics/tud_logo_rgb}}

% For remote only, center slide numbeing so that the speaker image (webcam) can be embedded
% in the bottom right
% \setbeamertemplate{footline}[text line]{\hspace*{\fill}\small \insertframenumber{} / \inserttotalframenumber\hspace*{\fill}}

\begin{document}
\maketitle

\frame{
    \frametitle{The Paper}
    \begin{itemize}
        \item I'm discussing this paper today:
    \end{itemize}
    \fullcite{KlWiHM2019}
    \cite{KlWiHM2019}
}

\section{Introduction}

\begin{frame}{Traditional Applications}
    \begin{figure}
        \includegraphics[width=\textwidth]{graphics/forever-alone-document-editing.png}
        \caption{Editing documents in peaceful solitude}
    \end{figure}
\end{frame}

\begin{frame}{“Cloud” Collaboration}
    \begin{figure}
        \includegraphics[scale=1.5]{graphics/cloud-collaboration-symbolic-image.png}
        \caption{Civilized and efficient collaboration}
    \end{figure}
\end{frame}

\begin{frame}{“Cloud” Collaboration Architecture}
    \begin{figure}
        \includegraphics[height=.8\textheight]{graphics/cloud-collaboration-architecture.pdf}
        \caption{
            Viewing, editing and saving documents in “cloud” collaboration
            systems
        }
    \end{figure}
\end{frame}

\begin{frame}{Problems}
    \begin{itemize}
        \item You are not in control
            \begin{itemize}
                \item Your data is on remote computers
                \item Your software runs on remote computers
            \end{itemize}
        \item Service providers have full access to all your data
        \item You are constrained by the provided APIs, user interfaces and
              terms \& conditions
        \item You depend on the ongoing operation of the cloud service
        \item Vendor lock-in, network effects, …
    \end{itemize}
\end{frame}

\section{Local-First Software}

\begin{frame}{Architecture}
    \begin{figure}
        \includegraphics[height=.8\textheight]{graphics/local-first-collaboration-architecture.pdf}
        \caption{
            Data replication in local-first collaboration systems
        }
    \end{figure}
\end{frame}

\begin{frame}{Properties}
    \begin{itemize}
        \item Avoid Latencies and Loading Times
        \begin{itemize}
            \item All the data is already on your device
            \item $\Rightarrow$ Near instantaneous access, no network roundtrips
            \item Non-blocking replication happens in the background
        \end{itemize}
        \pause
        \item Multi-Device Replication
        \begin{itemize}
            \item You can switch devices and resume your work seamlessly
        \end{itemize}
        \pause
        \item Working Offline Is Possible
        \begin{itemize}
            \item Working is always possible, regardless of being online or offline
            \item Replication happens when networks are available
        \end{itemize}
        \pause
        \item Seamless Collaboration
        \begin{itemize}
            \item Conflicts are handled gracefully
            \item Collaboration is streamlined with workflows
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Properties}
    \begin{itemize}
        \item Data Longevity
        \begin{itemize}
            \item The data and the software is locally on your device, so that
                documents can be accessed and interacted with in the future
        \end{itemize}
        \pause
        \item Privacy
        \begin{itemize}
            \item Data resides on your own device and is replicated only with
                end-to-end encryption
            \item Prevent third parties from exploiting your personal data
            \item Protect against leaks and breaches
            \item Indispensable in some domains: medical, journalists, diplomacy, …
        \end{itemize}
        \pause
        \item User Ownership and Control       
        \begin{itemize}
            \item You can copy, edit, process, reuse, distribute and migrate the
                data however you like
            \item This brings the responsibility of maintaining backups yourself
        \end{itemize}
    \end{itemize}
\end{frame}

\section{Implementation}

\begin{frame}{CRDTs}
    \begin{columns}
        \begin{column}{.5\textwidth}
            \begin{itemize}
                \item \textbf{C}onflict-Free/\textbf{C}ommutative \textbf{R}eplicated
                      \textbf{D}atatype
                \item Composable general-purpose data structures
                \item Support P2P-sync and guarantee eventual consistency
                \item Concurrent operations become commutative
            \end{itemize}
        \end{column}
        \begin{column}{.5\textwidth}
            \begin{figure}
                \includesvg[height=.6\textheight]{graphics/crdt-evolution.svg}
                \caption{
                    Concurrent evolution of a graphics CRDT
                    (taken from \cite{Baboulevitch2018})
                }
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Trellis}
    \begin{figure}
        \includegraphics[height=.8\textheight]{graphics/trellis.png}
        \caption{Local-first Kanban board \cite{KlWiHM2019}}
    \end{figure}
\end{frame}

\begin{frame}{PixelPusher}
    \begin{figure}
        \includegraphics[height=.8\textheight]{graphics/pixelpusher.png}
        \caption{Local-first collaborative drawing \cite{KlWiHM2019}}
    \end{figure}
\end{frame}

\begin{frame}{PushPin}
    \begin{figure}
        \includegraphics[height=.8\textheight]{graphics/pushpin.jpg}
        \caption{Local-first mixed media canvas \cite{KlWiHM2019}}
    \end{figure}
\end{frame}

\begin{frame}{Findings}
    \begin{itemize}
        \item CRDTs work reliably and are easy to integrate
        \item Offline editing worked well
        \item Conflicts were not a big problem, generic resolution strategies
              sufficed
        \item Very fine tracking of operations created performance bottlenecks
        \item P2P communication remains challenging
    \end{itemize}
\end{frame}

\begin{frame}{Open Research Problems}
    \begin{itemize}
        \item Branching/forking
        \item Schema migration
        \item Change management (workflows)
        \item Access management
        \item …
    \end{itemize}
\end{frame}

\section{Conclusion}

\begin{frame}{What Does the Future Look Like?}
    \begin{figure}
        \includegraphics[width=\textwidth]{graphics/future.pdf}
        \caption{Historical evolution of local- and remote-centric work}
    \end{figure}
\end{frame}

\frame{
    \printbibliography
}

\end{document}

% 	\begin{alertblock}{AlertBLock}
% 	\end{alertblock}

% 	\begin{exampleblock}{Beispiel}
% 	\end{exampleblock}
